Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ragg
Upstream-Contact: Thomas Lin Pedersen <thomas.pedersen@rstudio.com>
Source: https://cran.r-project.org/package=ragg

Files: *
Copyright: 2019-2020 Thomas Lin Pedersen, RStudio
License: Expat

Files: src/agg/*
Copyright: 2000-2005 Maxim Shemanarev (Author of AGG),
                     Tony Juricic (Contributor to AGG)
                     Milan Marusinec (Contributor to AGG)
                     Spencer Garrett (Contributor to AGG)
License: AGG
 Anti-Grain Geometry has dual licensing model. The Modified BSD
 License was first added in version v2.4 just for convenience.
 It is a simple, permissive non-copyleft free software license,
 compatible with the GNU GPL. It's well proven and recognizable.
 See http://www.fsf.org/licensing/licenses/index_html#ModifiedBSD
 for details.
 .
 Note that the Modified BSD license DOES NOT restrict your rights
 if you choose the Anti-Grain Geometry Public License.
 .
 .
 Anti-Grain Geometry Public License
 ====================================================
 .
 Anti-Grain Geometry - Version 2.4
 Copyright (C) 2002-2005 Maxim Shemanarev (McSeem)
 .
 Permission to copy, use, modify, sell and distribute this software
 is granted provided this copyright notice appears in all copies.
 This software is provided "as is" without express or implied
 warranty, and with no claim as to its suitability for any purpose.
 .
 .
 Modified BSD License
 ====================================================
 Anti-Grain Geometry - Version 2.4
 Copyright (C) 2002-2005 Maxim Shemanarev (McSeem)
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
 .
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
 .
  3. The name of the author may not be used to endorse or promote
     products derived from this software without specific prior
     written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

Files: debian/*
Copyright: 2020 Steffen Moeller <moeller@debian.org>
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
